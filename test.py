from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

driver = webdriver.Firefox()
driver.implicitly_wait(10) # seconds

driver.get("http://google.com")
print("Page title is: "+driver.title)
assert "Google" in driver.title

elem = driver.find_element(By.NAME, "q")
elem.clear()
elem.send_keys("SQR course is the best")
elem.send_keys(Keys.RETURN)


elem = driver.find_element(By.ID, "search")
assert elem.is_displayed()

elem = driver.find_element(By.NAME, "q")
assert elem.get_attribute("value") == "SQR course is the best"
assert "SQR course is the best" in driver.title

driver.close()

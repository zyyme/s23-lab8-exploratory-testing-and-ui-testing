import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options


options = Options()
options.headless = True

driver = webdriver.Remote(
        command_executor='http://selenium__standalone-firefox:4444/wd/hub',
        options=options
)
driver.implicitly_wait(10) # seconds

# Site selected: https://icons8.com/icons
# Test case 1: Getting icons by searching
driver.get('https://icons8.com/icons')

print('Page title is: '+driver.title)

elem = driver.find_element(By.ID, 'icons-search-input')
elem.clear()
elem.send_keys('cat')
elem.send_keys(Keys.RETURN)

time.sleep(2)

elem = driver.find_element(By.CLASS_NAME, 'search-page__content')
assert 'Cat' in elem.text
print('Test 1 passed')

# Test case 2: Getting icons by selecting popular
driver.get('https://icons8.com/icons')
print('Page title is: '+driver.title)

elems = driver.find_elements(By.CLASS_NAME, 'i8-link')

for elem in elems:
    if elem.text == 'phone':
        elem.click()
        break
    
time.sleep(4)

elem = driver.find_element(By.CLASS_NAME, 'search-page__content')
assert 'Phone' in elem.text
print('Test 2 passed')

# Test case 3: Getting icons by selecting style
driver.get('https://icons8.com/icons')
print('Page title is: '+driver.title)

elems = driver.find_elements(By.CLASS_NAME, 'platform-preview__image-block')
elems[0].click()

time.sleep(4)

elem = driver.find_element(By.CLASS_NAME, 'icons-platforms__grid')
assert 'Logos' in elem.text
print('Test 3 passed')

driver.close()